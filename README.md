# Nom
Prise de décision collective et vote - Groupe H

# Visuels
![alt](image/screen1.png)

Dans l'exemple ci-dessus, le nombre de votants a été initialisé à 5 pour tenir en une image à titre d'exemple.
Pour suivre l'avancée du vote et pour simuler de vrais agents votants, chaque agent avec ses préférences est affché dans la console, mais une fois le vote recu au niveau du Ballot, les votes sont anonymisés.

# Installation
Après téléchargement du projet, se placer dans le répertoire "ia04".

Le dossier "cmd" sert à lancer les tests sur le code, le dossier "comsoc" contient les différentes méthodes de votes et fonctions utilitaires, le dossier serv contient la partie serveur REST.

--> Pour lancer les tests une fois placé dans le dossier ia04, lancer la commande servtest "go run ./cmd/servtest". Note : une fois servtest lancé, la console propose la création d'un ballot, néanmoins, il est possible d'utiliser l'API afin de faire des opérations sur le serveur sans utiliser la console. 

--> Pour modifier le nombre d'agents votant, modifier la variable "numAgents" de "servtest"

# Utilisation

Cette application vous permet de mettre en oeuvre différentes méthodes de vote afin d'en comprendre le fonctionnement et de les comparer entre elles. Voici quelques utilisations possibles:

## Scénarios d'utilisation

### 1. Création d'Agents de Vote

Chaque agent représente un électeur potentiel. Ceux-ci participent à des élections en fournissant au serveur de vote une liste ordonnée de candidats selon leur préférence. 

### 2. Sélection des Méthodes de Vote

Ce programme permet le déroulement d'élection selon plusieurs méthode différentes (Majorité, Vote de Copeland, Borda...) Vous pouvez définir la méthode de vote à utiliser avant chaque élection.

### 3. Lancement des Élections

Chaque élection se déroule pendant une fenêtre de temps déterminée à l'avance. Lorsque le vote est ouvert, chaque agent peut partager sa liste de préférence afin d'élire un candidat. Une fois le vote fini, il est impossible pour un agent de voter. De même, il est impossible pour un agent de voter deux fois pour les mêmes élections.

### 4. Collecte des Résultats

Après avoir exécuté les élections, vous pouvez collecter les résultats pour évaluer comment chaque méthode de vote influence les résultats des élections.

A la fin de chaque élection, les résultats sont collectés par le serveur. Ces résultats sont ensuite analysé de manière anonyme afin de donner le résultat correspondant à la méthode de vote choisie.

EN CAS D'EGALITE : une fonction tie-break se déclenche pour départager les candidats selon un ordre arbitraire fourni dans le ballot, cela permet de définir un vainqueur sans ambiguïté (mais cela n'impactera pas la liste "ranking" finale qui restera brute en cas d'égalité).

## Exemple d'utilisation

1. Créez une centaine d'agent avec des préférences de vote définies.
2. Choisissez la méthode de vote "Majority".
3. Lancez l'élection.
4. Analysez les résultats obtenus.

# Auteur
Ce projet à été réalisé par Jean Lescieux et Raphaël Quintaneiro sous la supervision de Mr. Lagrue. Merci à ce dernier de nous avoir fourni les ressources nécessaire pour mener ce projet à terme.

# Licence MIT

Droit d'auteur (c) [2023] [Lescieux Jean, Quintaneiro Raphaël]

La permission est accordée, à titre gracieux, à toute personne obtenant une copie de ce logiciel et des fichiers de documentation associés (le "Logiciel"), de traiter le Logiciel sans restriction, y compris, sans s'y limiter, les droits d'utilisation, de copie, de modification, de fusion, de publication, de distribution, de sous-licence et/ou de vente de copies du Logiciel, et de permettre aux personnes auxquelles le Logiciel est fourni de le faire, sous réserve des conditions suivantes :

Le texte ci-dessus doit être inclus dans toutes les copies ou parties substantielles du Logiciel.

Le Logiciel est fourni "tel quel", sans garantie d'aucune sorte, expresse ou implicite, y compris, sans limitation, les garanties de qualité marchande, d'adéquation à un usage particulier et d'absence de contrefaçon. En aucun cas, les auteurs ou les titulaires des droits d'auteur ne sauraient être tenus pour responsables de toute réclamation, de tout dommage ou de toute autre responsabilité, que ce soit dans le cadre d'un contrat, d'un délit ou autre, découlant de, hors de ou en relation avec le Logiciel ou son utilisation ou d'autres opérations dans le Logiciel.


