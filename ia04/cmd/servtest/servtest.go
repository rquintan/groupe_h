package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.utc.fr/rquintan/ia04binome/Groupe_H/ia04/serv"
)

func main() {
	const numAgents = 100
	reader := bufio.NewReader(os.Stdin)
	http.HandleFunc("/new_ballot", serv.NewBallot)
	http.HandleFunc("/vote", serv.VoteForBallot)
	http.HandleFunc("/result", serv.GetResult)
	go func() {
		if err := http.ListenAndServe(":8080", nil); err != nil {
			log.Fatal(err)
		}
	}()

	for {
		err := serv.Start(numAgents)
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(2 * time.Second)
		fmt.Println()
		fmt.Print("Do you want to start another vote? (yes/no): ")
		text, _ := reader.ReadString('\n')
		if strings.TrimSpace(text) != "yes" {
			break
		}
	}
}
