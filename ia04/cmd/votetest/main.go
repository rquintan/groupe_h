package main

import (
	"fmt"

	"gitlab.utc.fr/rquintan/ia04binome/Groupe_H/ia04/comsoc"
)

func main() {
	// Définition des alternatives
	alt1 := comsoc.Alternative(1)
	alt2 := comsoc.Alternative(2)
	alt3 := comsoc.Alternative(3)
	alt4 := comsoc.Alternative(4)
	alt5 := comsoc.Alternative(5)
	alt6 := comsoc.Alternative(6)
	alt7 := comsoc.Alternative(7)
	alt8 := comsoc.Alternative(8)
	alt9 := comsoc.Alternative(9)
	alt10 := comsoc.Alternative(10)

	// Profil de préférences
	preferences := comsoc.Profile{
		{alt1, alt2, alt3, alt4, alt5, alt6, alt7, alt8, alt9, alt10},
		{alt6, alt7, alt8, alt9, alt10, alt1, alt2, alt3, alt4, alt5},
		{alt10, alt9, alt8, alt7, alt6, alt5, alt4, alt3, alt2, alt1},
		{alt5, alt4, alt3, alt2, alt1, alt6, alt7, alt8, alt9, alt10},
		{alt1, alt3, alt2, alt4, alt5, alt6, alt8, alt7, alt9, alt10},
		{alt6, alt8, alt7, alt9, alt10, alt5, alt4, alt3, alt2, alt1},
		{alt10, alt7, alt9, alt6, alt8, alt1, alt2, alt3, alt4, alt5},
		{alt5, alt2, alt4, alt3, alt1, alt6, alt7, alt8, alt9, alt10},
		{alt1, alt4, alt3, alt2, alt5, alt6, alt7, alt10, alt8, alt9},
		{alt6, alt7, alt10, alt8, alt9, alt1, alt2, alt3, alt4, alt5},
	}

	// Liste des alternatives
	//alternatives := []comsoc.Alternative{alt1, alt2, alt3, alt4, alt5, alt6, alt7, alt8, alt9, alt10}

	// Seuil pour le vote par approbation
	thresholds := []int{1, 2, 3, 1, 2, 3, 1, 2, 3, 1}

	// Test de MajoritySWF
	resMap, err := comsoc.MajoritySWF(preferences)
	if err != nil {
		fmt.Printf("Erreur dans MajoritySWF : %v\n", err)
	} else {
		fmt.Println("Résultat de MajortySWF :", resMap)
	}

	// Test de MajoritySCF
	resList, err := comsoc.MajoritySCF(preferences)
	if err != nil {
		fmt.Printf("Erreur dans MajoritySCF : %v\n", err)
	} else {
		fmt.Println("Résultat de MajoritySCF :", resList)
	}

	// Test de ApprovalSWF
	resMap, err = comsoc.ApprovalSWF(preferences, thresholds)
	if err != nil {
		fmt.Printf("Erreur dans ApprovalSWF : %v\n", err)
	} else {
		fmt.Println("Résultat de ApprovalSWF :", resMap)
	}

	// Test de ApprovalSCF
	resList, err = comsoc.ApprovalSCF(preferences, thresholds)
	if err != nil {
		fmt.Printf("Erreur dans ApprovalSCF : %v\n", err)
	} else {
		fmt.Println("Résultat de ApprovalSCF :", resList)
	}

	// Test de BordaSWF
	resMap, err = comsoc.BordaSWF(preferences)
	if err != nil {
		fmt.Printf("Erreur dans BordaSWF : %v\n", err)
	} else {
		fmt.Println("Résultat de BordaSWF :", resMap)
	}

	// Test de BordaSCF
	resList, err = comsoc.BordaSCF(preferences)
	if err != nil {
		fmt.Printf("Erreur dans BordaSCF : %v\n", err)
	} else {
		fmt.Println("Résultat de BordaSCF :", resList)
	}

	// Test de CondorcetWinner
	prefs1 := [][]comsoc.Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}
	resList, err = comsoc.CondorcetWinner(prefs1)
	if err != nil {
		fmt.Printf("Erreur : %v\n", err)
	} else {
		fmt.Printf("Gagnants de Condorcet : %v\n", resList)
	}

	// Test de CopelandSWF
	prefsX := [][]comsoc.Alternative{
		{1, 2, 3, 4, 5},
		{1, 3, 2, 4, 5},
		{2, 1, 4, 3, 5},
	}
	resMap, err = comsoc.CopelandSWF(prefsX)
	if err != nil {
		fmt.Printf("Erreur dans CopelandSWF : %v\n", err)
	} else {
		fmt.Println("Résultat de CopelandSWF :", resMap)
	}

	// Test de CopelandSCF
	resList, err = comsoc.CopelandSCF(prefsX)
	if err != nil {
		fmt.Printf("Erreur dans CopelandSCF : %v\n", err)
	} else {
		fmt.Println("Résultat de CopelandSCF :", resList)
	}

	// Test de STV_SWF
	resMap, err = comsoc.STV_SWF(prefsX)
	if err != nil {
		fmt.Printf("Erreur dans STV_SWF : %v\n", err)
	} else {
		fmt.Println("Résultat de STV_SWF :", resMap)
	}

	// Test de STV_SCF
	resList, err = comsoc.STV_SCF(preferences)
	if err != nil {
		fmt.Printf("Erreur dans STV_SCF : %v\n", err)
	} else {
		fmt.Println("Résultat de STV_SCF :", resList)
	}

}
