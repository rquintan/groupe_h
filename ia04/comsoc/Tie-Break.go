package comsoc

import "errors"

func TieBreak(alternatives []Alternative) (Alternative, error) {
	orderedAlts := []Alternative{1, 2, 3, 4, 5}

	for _, alt := range orderedAlts {
		for _, candidate := range alternatives {
			if candidate == alt {
				return alt, nil
			}
		}
	}

	return 0, errors.New("Aucune alternative convenable trouvée")
}

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alternatives []Alternative) (Alternative, error) {
		for _, alt := range orderedAlts {
			for _, candidate := range alternatives {
				if candidate == alt {
					return alt, nil
				}
			}
		}

		return 0, errors.New("Aucune alternative convenable trouvée")
	}
}

func WinnerIs(bestAlts []Alternative, tieBreakList []Alternative) ([]Alternative, error) {
	// cas avec un seul gagnant
	if len(bestAlts) == 1 {
		return bestAlts, nil
	}

	// Si plusieurs alternatives ont la même valeur maximale, on utilise le TieBreak
	orderedAlts := tieBreakList //On récupère du ballot
	tieBreakFunc := TieBreakFactory(orderedAlts)
	winner, err := tieBreakFunc(bestAlts)
	if err != nil {
		return nil, err
	}
	//create a list of Alternative and add the winner to return the right type
	winnerList := []Alternative{winner}
	return winnerList, err
}
