package comsoc

func CopelandSWF(p Profile) (count Count, err error) {
	count = make(Count)

	for _, alt1 := range p[0] {
		count[alt1] = 0
	}

	for _, alt1 := range p[0] {
		for _, alt2 := range p[0] {
			if alt1 != alt2 {
				wins := 0
				losses := 0
				for i := range p {
					if IsPref(alt1, alt2, p[i]) {
						wins++
					} else {
						losses++
					}
				}

				if wins > losses {
					count[alt1]++
				} else {
					count[alt1]--
				}
			}
		}
	}

	return count, nil
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)

	return MaxCount(count), nil
}
