package comsoc

func STV_SWF(p Profile) (count Count, err error) {
	count = make(Count)
	pNew := p
	var del []Alternative

	i := 0
	for len(pNew[0]) >= 1 {
		// fmt.Printf("Tour %d\n", i)
		// fmt.Println("pNew:", pNew)

		score, _ := MajoritySWF(pNew)
		// fmt.Println("score:", score)

		del = MinCount(score)
		// fmt.Println("del:", del)

		for _, b := range del {
			count[b] = i
		}
		// fmt.Println("count:", count)

		pNew = RemoveAlternatives(pNew, del)
		// fmt.Println("pNew après suppression:", pNew)

		i++
	}

	return count, nil
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := STV_SWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), nil
}
