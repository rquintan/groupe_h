package comsoc

import (
	"fmt"
	"math"
)

// renvoie l'indice ou se trouve alt dans prefs
func Rank(alt Alternative, prefs []Alternative) int {
	for i := range prefs {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func IsPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return Rank(alt1, prefs) < Rank(alt2, prefs)
}

// renvoie le meilleur resultat d'un map
func MaxCount(count Count) (bestAlts []Alternative) {
	var maxScore int = -(1 << 31)
	for _, score := range count {
		if score > maxScore {
			maxScore = score
		}
	}

	for key, score := range count {
		if score == maxScore {
			bestAlts = append(bestAlts, key)
		}
	}

	return bestAlts
}

// renvoie le pire resultat d'un map
func MinCount(count Count) (bestAlts []Alternative) {
	var minScore int = math.MaxInt32
	for _, score := range count {
		if score < minScore {
			minScore = score
		}
	}

	for key, score := range count {
		if score == minScore {
			bestAlts = append(bestAlts, key)
		}
	}

	return bestAlts
}

// vérifie les préférences d'un agent
func CheckProfile(prefs []Alternative, alts []Alternative) error {
	altCount := make(map[Alternative]int)

	for _, alt := range prefs {
		if !contains(alts, alt) {
			return fmt.Errorf("L'alternative %d n'existe pas.", alt)
		}
		altCount[alt]++
	}

	for alt, count := range altCount {
		if count > 1 {
			return fmt.Errorf("L'alternative %d apparaît plus d'une fois.", alt)
		}
		if count == 0 {
			return fmt.Errorf("L'alternative %d n'est pas dans la liste.", alt)
		}
	}

	return nil
}

// vérifie si une alternative existe dans la liste
func contains(alts []Alternative, alt Alternative) bool {
	for _, a := range alts {
		if a == alt {
			return true
		}
	}
	return false
}

// vérifie le profil
func CheckProfileAlternative(prefs Profile, alts []Alternative) error {
	for i := range prefs {
		if err := CheckProfile(prefs[i], alts); err != nil {
			return err
		}
	}
	return nil
}

// supprime une alternative d'un profil
func RemoveAlternatives(p Profile, toRemove []Alternative) Profile {
	var newProfile Profile

	for _, prefs := range p {
		var newPrefs []Alternative
		for _, alt := range prefs {
			remove := false
			for _, removeAlt := range toRemove {
				if alt == removeAlt {
					remove = true
					break
				}
			}
			if !remove {
				newPrefs = append(newPrefs, alt)
			}
		}
		newProfile = append(newProfile, newPrefs)
	}

	return newProfile
}
