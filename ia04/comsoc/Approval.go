package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	count = make(Count)

	for tr, prefs := range p {
		for i, alt := range prefs {
			if i < thresholds[tr] {
				count[alt]++
			}
		}
	}

	return count, nil
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}

	return MaxCount(count), nil
}
