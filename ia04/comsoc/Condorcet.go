package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	for _, alt1 := range p[0] {
		isCondorcetWinner := true

		for _, alt2 := range p[0] {
			if alt1 != alt2 {
				wins := 0
				losses := 0
				for i := range p {
					if IsPref(alt1, alt2, p[i]) {
						wins++
					} else {
						losses++
					}
				}

				if losses >= wins {
					isCondorcetWinner = false
					break
				}
			}
		}

		if isCondorcetWinner {
			bestAlts = append(bestAlts, alt1)
		}
	}

	return bestAlts, nil
}
