package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	// err = CheckProfileAlternative(p, alts)
	// if err != nil {
	// 	return nil, err
	// }

	count = make(Count)

	for _, alt := range p[0] {
		count[alt] = 0
	}

	for _, prefs := range p {
		firstChoice := prefs[0]
		count[firstChoice]++
	}

	return count, nil
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), nil
}
