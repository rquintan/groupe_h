package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	count = make(Count)

	numAlternatives := len(p[0])

	for _, alt := range p[0] {
		count[alt] = 0
	}

	for _, prefs := range p {
		for i, alt := range prefs {
			count[alt] += numAlternatives - i - 1
		}
	}

	return count, nil
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}
	return MaxCount(count), nil
}
