package serv

import "time"

type Agent struct {
	ID          string `json:"agent-id"`
	Preferences []int  `json:"prefs"`
	Option      int    `json:"option"`
}

type Ballot struct {
	ID       string    `json:"ballot-id"`
	Rule     string    `json:"rule"`
	Deadline time.Time `json:"deadline"`
	VoterIDs []string  `json:"voter-ids"`
	NumAlts  int       `json:"#alts"`
	TieBreak []int     `json:"tie-break"`
	Votes    [][]int
	Options  []int
}

type Vote struct {
	AgentID  string `json:"agent-id"`
	BallotID string `json:"ballot-id"`
	Prefs    []int  `json:"prefs"`
	Option   int    `json:"option"`
}

type Result struct {
	Winner  []int `json:"winner"`
	Ranking []int `json:"ranking"`
}
