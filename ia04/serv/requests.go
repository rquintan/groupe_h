package serv

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"time"

	"gitlab.utc.fr/rquintan/ia04binome/Groupe_H/ia04/comsoc"
)

func NewBallot(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received a request to create a new ballot.")
	var newBallot Ballot
	err := json.NewDecoder(r.Body).Decode(&newBallot)
	if err != nil {
		http.Error(w, "Error reading request body.", http.StatusBadRequest)
		return
	}

	ballotID := newBallot.ID
	// Register the ballot.
	ballots[ballotID] = &newBallot

	// Respond with the created ballot or an error message.
	if _, ok := ballots[ballotID]; ok {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(ballotID)
	} else {
		http.Error(w, "Error creating ballot.", http.StatusBadRequest)
	}
}

func VoteForBallot(w http.ResponseWriter, r *http.Request) {
	var vote Vote
	err := json.NewDecoder(r.Body).Decode(&vote)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	ballot, exists := ballots[vote.BallotID]
	if !exists {
		http.Error(w, "Not Implemented", http.StatusNotImplemented)
		return
	}

	if time.Now().After(ballot.Deadline) {
		http.Error(w, "Deadline has passed", http.StatusServiceUnavailable)
		return
	}

	for _, existingVote := range ballot.VoterIDs {
		if existingVote == vote.AgentID {
			http.Error(w, "Vote already made", http.StatusForbidden)
			return
		}
	}

	ballot.Votes = append(ballot.Votes, vote.Prefs)
	ballot.VoterIDs = append(ballot.VoterIDs, vote.AgentID)
	ballot.Options = append(ballot.Options, vote.Option)

	w.WriteHeader(http.StatusOK)
	fmt.Printf("Received a vote from Agent %s for Ballot %s with preferences: %v and option: %d\n", vote.AgentID, vote.BallotID, vote.Prefs, vote.Option)
}

func GetResult(w http.ResponseWriter, r *http.Request) {
	var ballotID string
	err := json.NewDecoder(r.Body).Decode(&ballotID)
	if err != nil {
		http.Error(w, "Error reading request body.", http.StatusBadRequest)
		return
	}
	// Check if the ballot exists.
	ballot, exists := ballots[ballotID]
	if !exists {
		http.Error(w, "Ballot not found.", http.StatusNotFound)
		return
	}
	// Check if the deadline has passed.
	if time.Now().Before(ballot.Deadline) {
		http.Error(w, "The deadline has not passed yet.", http.StatusTooEarly)
		return
	}

	// Convert [][]int to comsoc.Profile
	profile := make(comsoc.Profile, len(ballot.Votes))
	for i, v := range ballot.Votes {
		profile[i] = make([]comsoc.Alternative, len(v))
		for j, alt := range v {
			profile[i][j] = comsoc.Alternative(alt)
		}
	}

	// Check if the profile is valid
	alternatives := make([]comsoc.Alternative, len(ballot.TieBreak))
	for i, alt := range ballot.TieBreak {
		alternatives[i] = comsoc.Alternative(alt)
	}

	err = comsoc.CheckProfileAlternative(profile, alternatives)
	if err != nil {
		http.Error(w, "Profile is not valid", http.StatusMethodNotAllowed)
		return
	}

	// Get the winners
	result, err := getWinners(ballot.Rule, profile, ballot.Options, alternatives)
	if err != nil {
		http.Error(w, fmt.Sprintf("%v", err), http.StatusMethodNotAllowed)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func getWinners(rule string, profile comsoc.Profile, options []int, alternatives []comsoc.Alternative) (Result, error) {
	var winnerSCF []int
	var winnerSWF []int

	switch rule {
	case "Majority":
		bestAltsSCF, err := comsoc.MajoritySCF(profile)
		if err != nil {
			return Result{}, err
		}
		bestAltsSWF, err := comsoc.MajoritySWF(profile)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerPreTB, err := comsoc.WinnerIs(bestAltsSCF, alternatives)
		winnerSCF = transformSCFWinners(winnerPreTB)
		winnerSWF = transformSWFWinners(bestAltsSWF)
	case "Condorcet":
		bestAltsSCF, err := comsoc.CondorcetWinner(profile)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerSCF = transformSCFWinners(bestAltsSCF)
		winnerSWF = winnerSCF
	case "Borda":
		bestAltsSCF, err := comsoc.BordaSCF(profile)
		if err != nil {
			return Result{}, err
		}
		bestAltsSWF, err := comsoc.BordaSWF(profile)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerPreTB, err := comsoc.WinnerIs(bestAltsSCF, alternatives)
		winnerSCF = transformSCFWinners(winnerPreTB)
		winnerSWF = transformSWFWinners(bestAltsSWF)
	case "Copeland":
		bestAltsSCF, err := comsoc.CopelandSCF(profile)
		if err != nil {
			return Result{}, err
		}
		bestAltsSWF, err := comsoc.CopelandSWF(profile)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerPreTB, err := comsoc.WinnerIs(bestAltsSCF, alternatives)
		winnerSCF = transformSCFWinners(winnerPreTB)
		winnerSWF = transformSWFWinners(bestAltsSWF)
	case "STV":
		bestAltsSCF, err := comsoc.STV_SCF(profile)
		if err != nil {
			return Result{}, err
		}
		bestAltsSWF, err := comsoc.STV_SWF(profile)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerPreTB, err := comsoc.WinnerIs(bestAltsSCF, alternatives)
		winnerSCF = transformSCFWinners(winnerPreTB)
		winnerSWF = transformSWFWinners(bestAltsSWF)
	case "Approval":
		bestAltsSCF, err := comsoc.ApprovalSCF(profile, options)
		if err != nil {
			return Result{}, err
		}
		bestAltsSWF, err := comsoc.ApprovalSWF(profile, options)
		if err != nil {
			return Result{}, err
		}
		// Tie-Break
		winnerPreTB, err := comsoc.WinnerIs(bestAltsSCF, alternatives)
		winnerSCF = transformSCFWinners(winnerPreTB)
		winnerSWF = transformSWFWinners(bestAltsSWF)
	default:
		return Result{}, fmt.Errorf("unknown rule %s", rule)
	}

	return Result{Winner: winnerSCF, Ranking: winnerSWF}, nil
}

func transformSCFWinners(bestAlts []comsoc.Alternative) []int {
	winners := make([]int, len(bestAlts))
	for i, alt := range bestAlts {
		winners[i] = int(alt)
	}
	return winners
}

func transformSWFWinners(count comsoc.Count) []int {
	type AltScore struct {
		Alt   comsoc.Alternative
		Score int
	}

	var altScores []AltScore
	for alt, score := range count {
		altScores = append(altScores, AltScore{Alt: alt, Score: score})
	}

	sort.Slice(altScores, func(i, j int) bool {
		return altScores[i].Score > altScores[j].Score
	})

	var winners []int
	for _, altScore := range altScores {
		winners = append(winners, int(altScore.Alt))
	}

	return winners
}
