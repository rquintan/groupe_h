package serv

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"
)

var ballots = make(map[string]*Ballot)
var VotedCount int // Number of agents that have voted

func Start(numAgents int) error {
	VotedCount = 0
	// Create a new ballot.
	ballot, err := CreateBallot()
	if err != nil {
		return fmt.Errorf("Error creating ballot: %s", err)
	}

	// Start agents voting
	agents := InitializeAgents(numAgents, ballot.NumAlts)
	for _, agent := range agents {
		go func(a Agent) {
			duration := ballot.Deadline.Sub(time.Now())
			initialDuration := duration
			duration += time.Duration(float64(initialDuration) / 5)
			randomDelay := time.Duration(rand.Int63n(int64(duration)))
			time.Sleep(randomDelay)
			vote := Vote{
				AgentID:  a.ID,
				BallotID: ballot.ID,
				Prefs:    a.Preferences,
				Option:   a.Option,
			}
			sendVote(vote)
		}(agent)
	}

	// Wait for the voting to complete.
	timeRemaining := time.Until(ballot.Deadline)
	time.Sleep(timeRemaining)

	// Get the results.
	GetResults(ballot.ID)
	fmt.Println("Voted:", VotedCount, " Didn't vote:", numAgents-VotedCount)
	return nil
}

func CreateBallot() (Ballot, error) {
	// Ask the user for the ballot rule, deadline in seconds, and number of alternatives.
	var rule string
	fmt.Print("Enter the ballot rule: ")
	fmt.Scanln(&rule)
	var duration int
	fmt.Print("Enter the ballot duration in seconds: ")
	fmt.Scanln(&duration)
	var numAlts int
	fmt.Print("Enter the number of alternatives: ")
	fmt.Scanln(&numAlts)

	//Create a list of int from 1 to numAlts.
	TieBreak := make([]int, numAlts)
	for i := 0; i < numAlts; i++ {
		TieBreak[i] = i + 1
	}

	// Create a new ballot request body.
	newBallot := Ballot{
		ID:       fmt.Sprintf("ballot%d", len(ballots)+1),
		Rule:     rule,
		Deadline: time.Now().Add(time.Duration(duration) * time.Second),
		NumAlts:  numAlts,
		TieBreak: TieBreak,
		Votes:    [][]int{},
		VoterIDs: []string{},
	}

	//Print the ballot
	fmt.Printf("Ballot created %v\n", newBallot)

	// Send a request to create the new ballot.
	reqBody, err := json.Marshal(newBallot)
	if err != nil {
		return Ballot{}, err
	}
	resp, err := http.Post("http://localhost:8080/new_ballot", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		return Ballot{}, err
	}
	defer resp.Body.Close()

	// Check the response status code.
	if resp.StatusCode == http.StatusOK {
		var BallotID string
		err := json.NewDecoder(resp.Body).Decode(&BallotID)
		if err == nil {
			fmt.Printf("Ballot created with ID: %s\n", BallotID)
		}
		resp.Body.Close()
	} else {
		return Ballot{}, fmt.Errorf("Error creating ballot: %s", resp.Status)
	}

	// if the ballot has been created successfully, return it.
	return newBallot, nil
}

func InitializeAgents(numAgents, numPreferences int) []Agent {
	agents := make([]Agent, numAgents)
	for i := 0; i < numAgents; i++ {
		agent := Agent{
			ID:          fmt.Sprintf("agent%d", i+1),
			Preferences: generateRandomPreferences(numPreferences),
			Option:      rand.Intn(numPreferences-1) + 1}
		agents[i] = agent
	}
	return agents
}

func generateRandomPreferences(numPreferences int) []int {
	preferences := rand.Perm(numPreferences)
	for i := range preferences {
		preferences[i]++
	}
	return preferences
}

func sendVote(vote Vote) {
	// Simulate agent sending a vote.
	url := fmt.Sprintf("http://localhost:8080/vote")
	data, _ := json.Marshal(vote)

	resp, _ := http.Post(url, "application/json", bytes.NewBuffer(data))
	resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		VotedCount++
	}
}

func GetResults(ballotID string) {
	url := fmt.Sprintf("http://localhost:8080/result")
	data, _ := json.Marshal(ballotID)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		log.Fatalf("Failed to send request: %v", err)
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(io.Reader(resp.Body))
	if err != nil {
		log.Fatalf("Failed to read response body: %v", err)
	}

	if resp.StatusCode == http.StatusOK {
		var result Result
		err := json.Unmarshal(bodyBytes, &result)
		if err != nil {
			log.Fatalf("Failed to unmarshal response body: %v", err)
		}
		fmt.Printf("Results received. Winner: %d\n", result.Winner)
		fmt.Printf("                  Ranking: %d\n", result.Ranking)
	} else {
		fmt.Printf("Failed to retrieve results: %s", string(bodyBytes))
	}
}
